# roelink.eu

This is the git repository of my website.

## Start the development server

```shell
npm run dev
```

## Build

```shell
npm run generate
```
