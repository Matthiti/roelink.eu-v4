module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}"
  ],
  theme: {
    extend: {},
    container: {
      center: true,
      screens: {
        xl: '1140px',
        lg: '960px',
        md: '768px',
        sm: '576px',
      },
      padding: '15px'
    }
  },
  plugins: [],
}
